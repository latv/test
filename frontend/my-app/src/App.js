import React from 'react';
import './App.css';
import { Route, BrowserRouter as Router } from 'react-router-dom';
import Product from './Pages/Product';
import ProductAdd from './Pages/ProductAdd';
function App() {
  return (
    <>
      <Router>
        <Route exact path="/add" component={ProductAdd} />
        <Route exact path="/" component={Product} />
      </Router>
    </>
  );
}

export default App;
