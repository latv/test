import axios from 'axios';


const request = async (url, data, method, isAuthorized = true) => {

  const requestConfig = {
    url: url,
    method: method,
    baseURL: process.env.REACT_APP_BACKEND_URL,
    responseType: 'json',
    // headers: {'Authorization': jwt.getHeader()}
  };

  if (method === 'GET') {
    requestConfig.params = data;
  } else {
    requestConfig.data = data;
  }

  try {
    const response = await axios.request(requestConfig);

    console.log(response);

    return response.data;


  } catch (e) {
    console.log(e);


  }
}

export default { request };