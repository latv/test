import React from 'react';
import './styles.scss';


const Card = ({SKU,name,value,special,switcher}) => {
const specialAtribute = (switcher,value) => {switch (switcher) {
    case '1':
        return 'Size '+value;
        break;

    case '2':
        return 'Weight '+value;
        break;
    case '3':
        return 'Dimension '+value;
        break;
    default:
        return 'error';
}}
    return (
     

        
        <div className='card text-center'>
            <div className='overflow'>
                <h1>{SKU}</h1>
            </div>
            <div className='card-body text-dark'>
                <h4 className='card-title'>{name}</h4>
                <p className='card-text text-secondary'>
                    {value} $
                </p>
                <p className=''>{specialAtribute(switcher,special)}</p>
            </div>
            </div>
           
    )

}
export default Card;