import React, { useState, useEffect } from 'react';
import Card from './Card';

import APIClient from '../../utils/apiClient';



const Cards = () => {
    const [Items, setItems] = useState([]);
    const [isLoading, setIsLoading] = useState(true);


    useEffect(() => {


        getItems();
      }, []);



    const getItems = async () => {
        setIsLoading(true);
        let response = await APIClient.request(
            'http://localhost/test_scandiweb/api/post/read.php',
            {},
            'GET'
        );

        console.log(response);

        setItems(response);
        setIsLoading(false);
    };

    const listItems = () =>{
        
            try {
            const card =  Items.map((item) => <Card SKU={item.SKU} name={item.name} value={item.value} switcher={item.switch} special={item.switchValue} loading={isLoading}/>);
            return card;
            }catch(e){console.log(e);}
}

    


    return (

        <div className="container-fluid d-flex justify-center">
            <div className="row">
  
                    {listItems()}
      
            </div>
        </div>
     
    )

}
export default Cards;