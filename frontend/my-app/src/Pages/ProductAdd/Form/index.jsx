import React from 'react';
import { Form } from 'react-bootstrap';
import './styles.scss';




const Forms = ({ setValue, setName, setSKU,
    setSwitcher, switcher, setSize, setLength, setHeight, setWidth, setWeight }) => {






    const specialForm = () => {
        const nav = { switcher }
        console.log(nav.switcher);
        try {
            if (nav.switcher == 1) {

                return <Form.Group controlId="exampleForm.ControlInput1"
                    onSubmit={e => { e.preventDefault(); }} className='input_field'>
                    <Form.Label>size</Form.Label>
                    <Form.Control type="text" placeholder="size"
                        onSubmit={e => { e.preventDefault(); }} onChange={(el) => setSize(el.target.value)} />
                </Form.Group>;

            }
            if (nav.switcher == 2) {

                return <Form.Group controlId="exampleForm.ControlInput1"
                    onSubmit={e => { e.preventDefault(); }} className='input_field'>
                    <Form.Label>weight</Form.Label>
                    <Form.Control type="text" placeholder="weight" onSubmit={e => { e.preventDefault(); }}
                        onChange={(el) => setWeight(el.target.value)} />
                </Form.Group>;

            }
            if (nav.switcher == 3) {

                return <Form.Group controlId="exampleForm.ControlInput1"
                    onSubmit={e => { e.preventDefault(); }} className='input_field'>
                    <Form.Label>Height</Form.Label>
                    <Form.Control type="text" placeholder="Name"
                        onSubmit={e => { e.preventDefault(); }} onChange={(el) => setHeight(el.target.value)} />
                    <Form.Label>Width</Form.Label>
                    <Form.Control type="text" placeholder="Name"
                        onSubmit={e => { e.preventDefault(); }} onChange={(el) => setWidth(el.target.value)} />
                    <Form.Label>Length</Form.Label>
                    <Form.Control type="text" placeholder="Name"
                        onSubmit={e => { e.preventDefault(); }} onChange={(el) => setLength(el.target.value)} />
                </Form.Group>;

            }
        } catch (err) { console.log(err); }
    }


    return (

        <>
            <Form>
                <Form.Group controlId="exampleForm.ControlInput1" className='input_field'>
                    <Form.Label>SKU</Form.Label>
                    <Form.Control type="text" placeholder="SKU"
                        onSubmit={e => { e.preventDefault(); }} onChange={(el) => setSKU(el.target.value)} />
                </Form.Group>
                <Form.Group controlId="exampleForm.ControlInput1"
                    onSubmit={e => { e.preventDefault(); }} className='input_field'>
                    <Form.Label>Name</Form.Label>
                    <Form.Control type="text" placeholder="Name"
                        onSubmit={e => { e.preventDefault(); }} onChange={(el) => setName(el.target.value)} />
                </Form.Group>
                <Form.Group controlId="exampleForm.ControlInput1" className='input_field'>
                    <Form.Label >Price</Form.Label>
                    <Form.Control type="text" placeholder="Price"
                        onSubmit={e => { e.preventDefault(); }} name='price' className="form-control"
                        onChange={(el) => setValue(el.target.value)} />
                    <Form.Label>Type Switcher</Form.Label>
                    <Form.Control as="select" size="sm"
                        onChange={(el) => setSwitcher(el.target.value)} custom>
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                    </Form.Control>
                </Form.Group>
                {specialForm()}
            </Form>
        </>
    )

}
export default Forms;