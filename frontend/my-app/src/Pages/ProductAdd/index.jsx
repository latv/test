import React, { useState } from 'react';

import './styles.scss';
import APIClient from '../../utils/apiClient';
import { Row, Col, Layout, Button } from 'antd';
import Forms from './Form';


const { Header, Content, Footer } = Layout;
const xsWidth = 22;
const mdWidth = 18;
const lgWidth = 16;
const ProductAdd = () => {
  const [value, setValue] = useState("");
  const [name, setName] = useState("");
  const [SKU, setSKU] = useState("");
  const [size, setSize] = useState(0);
  const [weight, setWeight] = useState(0);

  const [height, setHeight] = useState(0);
  const [width, setWidth] = useState(0);
  const [lenth, setLength] = useState(0);

  const [switcher, setSwitcher] = useState(1);
  const sendItem = async () => {
    var specialAtribute = '';
    switch (switcher) {
      case 1:
        specialAtribute = String(size);
        break;

      case 2:
        specialAtribute = String(weight)
        break;
      case 3:
        specialAtribute = String(height) + "x" + String(width) + "x" + String(lenth);
        break;
    }
    let response = await APIClient.request(
      'http://localhost/test_scandiweb/api/post/create.php',
      {
        "SKU": SKU,
        "name": name,
        "value":  parseFloat(value),
        "switch": parseFloat(switcher),
        "switchValue": specialAtribute
      },
      'POST'
    );

    console.log(response);

    if (response === null){
      alert("something went wrong check your form");
    }
   
  }

  return (
    <>
      <Layout className="min-h-100">
        <Header className="app-header">
          <Row justify="center" >
            <Col xs={xsWidth} md={mdWidth} lg={lgWidth}>
              <p1 style={{ fontSize: '40px', padding: "10px", color: 'black' }} className="brand-logo" >Product Add</p1>
              <div className="app-header-content">
                <Button type="primary" onClick={sendItem}>Save</Button>
              </div>
            </Col>
          </Row>
          <hr />
        </Header>
        <Content className="app-content">
          <Row justify="center" >
            <Col xs={xsWidth} md={mdWidth} lg={lgWidth}>
              <Forms
                setValue={setValue}
                setName={setName}
                setSKU={setSKU}
                setSwitcher={setSwitcher}
                switcher={switcher}
                setSize={setSize}
                setWeight={setWeight}
                setHeight={setHeight}
                setWidth={setWidth}
                setLength={setLength}
              />
            </Col>
          </Row>
        </Content>
        <Footer className="app-footer">
          <Row justify="center">

          </Row>
        </Footer>

      </Layout>
    </>
  );

}
export default ProductAdd;