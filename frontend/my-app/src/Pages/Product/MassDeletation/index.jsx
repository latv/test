import React from 'react';
import {Button} from 'antd';
import './styles.scss';
import APIClient from '../../../utils/apiClient';
const MassDeletion = () => {
    const getDeleted = async () => {
        let response = await APIClient.request(
          'http://localhost/test_scandiweb/api/post/deleteAll.php',
          {},
          'POST'
        );
    
        console.log(response);
        document.location.reload(true);
        
      }
    return (
        <>
        <div className="delete">Mass deletion </div>
        <Button onClick={getDeleted}>Apply</Button>
        </>
    )

}
export default MassDeletion;