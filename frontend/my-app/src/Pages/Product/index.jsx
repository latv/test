import React from 'react';
import { Layout, Row, Col } from 'antd';
import Cards from '../../components/Cards'
import './styles.scss';
import MassDeletion from './MassDeletation';
const { Header, Content, Footer } = Layout;
const xsWidth = 22;
const mdWidth = 18;
const lgWidth = 16;





const Product = () => {

  return (

    <>

      <Layout className="min-h-100">
        <Header className="app-header">
          <Row justify="center" >
            <Col xs={xsWidth} md={mdWidth} lg={lgWidth}>
              <p1 style={{ fontSize: '40px', padding: "10px", color: 'black' }} className="brand-logo" >Product list</p1>
              <div className="app-header-content">
                
                <MassDeletion />
              </div>
            </Col>
          </Row>
          <hr />
        </Header>
        <Content className="app-content">
          <Row justify="center" >
            <Col xs={xsWidth} md={mdWidth} lg={lgWidth}>
              <Cards />
            </Col>
          </Row>
        </Content>
        <Footer className="app-footer">
          <Row justify="center">
          </Row>
        </Footer>
      </Layout>
    </>

  )

}
export default Product;