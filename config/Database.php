<?php 
  class Database {
    // DB Params
    private $host = 'localhost';
    private $db_name = 'itemslist';
    private $username = 'root';
    private $password = '';
    private $conn;

    // DB Connect
    public function connect() {
      $this->conn = null;

      try { 
        $this->conn = new PDO('mysql:host=' . $this->host . ';dbname=' . $this->db_name, $this->username, $this->password);
        $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql = "CREATE TABLE IF NOT EXISTS item_test (
            id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
            SKU VARCHAR(30) NOT NULL UNIQUE,
            name VARCHAR(30) NOT NULL,
            value DECIMAL(19, 2),
            switch INT(6),
            switchValue VARCHAR(30) NOT NULL
            )";
        
            // use exec() because no results are returned
    $this->conn->exec($sql);
            
            
      } catch(PDOException $e) {
        echo 'Connection Error: ' . $e->getMessage();
      }

      return $this->conn;
    }
  }