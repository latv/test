<?php 
  class Post {
    // DB stuff
    private $conn;
    private $table = 'item_test';

    // Post Properties
    public $id;

    public $SKU;
    
    public $name;
    public $value;
    public $switch;
    public $switchValue;

    // Constructor with DB
    public function __construct($db) {
      $this->conn = $db;
    }

    // Get Posts
    public function read() {
      // Create query
      $query = 'SELECT * FROM ' . $this->table ;
      
      // Prepare statement
      $stmt = $this->conn->prepare($query);

      // Execute query
      $stmt->execute();

      return $stmt;
    }


   

    // Create Post
    public function create() {
          // Create query
          $query = 'INSERT INTO ' . $this->table . ' SET SKU = :SKU, name = :name, value = :value, switch = :switch, switchValue = :switchValue';

          // Prepare statement
          $stmt = $this->conn->prepare($query);

          // Clean data
          $this->SKU = htmlspecialchars(strip_tags($this->SKU));
          $this->name = htmlspecialchars(strip_tags($this->name));
          $this->value = htmlspecialchars(strip_tags($this->value));
          $this->switch = htmlspecialchars(strip_tags($this->switch));
          $this->switchValue = htmlspecialchars(strip_tags(($this->switchValue)));

          // Bind data
          $stmt->bindParam(':SKU', $this->SKU);
          $stmt->bindParam(':name', $this->name);
          $stmt->bindParam(':value', $this->value);
          $stmt->bindParam(':switch', $this->switch);
          $stmt->bindParam(':switchValue',$this->switchValue);
          // Execute query
          if($stmt->execute()) {
            return true;
      }

      // Print error if something goes wrong
      echo json_decode ($stmt->error);

      return false;
    }

    public function delete() {
      // Create query
      $query = 'DELETE FROM ' . $this->table ;

      // Prepare statement
      $stmt = $this->conn->prepare($query);


      // Execute query
      if($stmt->execute()) {
        return true;
      }

      // Print error if something goes wrong
      printf("Error: %s.\n", $stmt->error);

      return false;
}


    
  }